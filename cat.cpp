///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   20 Apr 2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>
#include <cstdlib>

#define CAT_NAMES_FILE "names.txt"
#define nameLen 6

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}

bool CatEmpire::empty() {
   if(topCat == nullptr)   // if root Cat is null, list is empty
      return true;
   else
      return false;
}

void CatEmpire::addCat( Cat* newCat ) {
   assert( newCat != nullptr );  

   newCat->left = nullptr;    // should already be null
   newCat->right = nullptr;

   if( topCat == nullptr ) {  // if BST is empty, newCat is root Cat
      topCat = newCat;

      return;
   }

   addCat( topCat, newCat );  // if not empty, insert cat in correct branch
}

void CatEmpire::addCat( Cat* atCat, Cat* newCat){
   assert( atCat != nullptr );
   assert( newCat != nullptr );

   // traverse left if current name is greater than new name
   if( atCat->name > newCat->name ) {
      if( atCat->left == nullptr ) {
         atCat->left = newCat;
      } else {
         addCat( atCat->left, newCat );
      }
   }

   // traverse right if current name is less than new name
   if( atCat->name < newCat->name ) {
      if( atCat->right == nullptr ) {
         atCat->right = newCat;
      } else {
         addCat( atCat->right, newCat);
      }
   }
}

void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorderReverse( topCat, 1 );
}

void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const {
   if( atCat == nullptr )
      return;

   dfsInorderReverse( atCat->right, depth + 1 );

   cout << string( nameLen * (depth-1), ' ' ) << atCat->name;
   
   if( atCat->left == nullptr && atCat->right == nullptr )
      cout << endl;
   if( atCat->left != nullptr && atCat->right != nullptr )
      cout << '<' << endl;
   if( atCat->left != nullptr && atCat->right == nullptr )
      cout << '\\' << endl;
   if( atCat->left == nullptr && atCat->right != nullptr )
      cout << '/' << endl;

   dfsInorderReverse( atCat->left, depth + 1 );
}

void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorder( topCat );
}

void CatEmpire::dfsInorder( Cat* atCat ) const {
   if( atCat == nullptr )
      return;

   dfsInorder( atCat->left );

   cout << atCat->name << endl;
   
   dfsInorder( atCat->right );
}

void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsPreorder( topCat );
}

void CatEmpire::dfsPreorder( Cat* atCat ) const {
   if( atCat == nullptr )
      return;

   if( atCat->left != nullptr && atCat->right != nullptr )
      cout << atCat->name << " begat " << atCat->left->name << " and " << atCat->right->name << endl;
   if( atCat->left != nullptr && atCat->right == nullptr )
      cout << atCat->name << " begat " << atCat->left->name << endl;
   if( atCat->left == nullptr && atCat->right != nullptr )
      cout << atCat->name << " begat " << atCat->right->name << endl;

   dfsPreorder( atCat->left );
   
   dfsPreorder( atCat->right );
}

void CatEmpire::catGenerations() const {
   queue<Cat*> catQueue;
   int level = 1;

   // add topCat to queue and denote new level with nullptr
   catQueue.push( topCat );
   catQueue.push( nullptr );

   cout << level << getEnglishSuffix( level ) << " Generation" << endl; 

   while ( !catQueue.empty() ) {    // until all cats have gone through the queue...
      Cat* cat = catQueue.front();  // current cat is cat at front of queue
      catQueue.pop();               // remove front cat after saving it's value
      
      if( cat == nullptr ) {  // nullptr indicates new level
         level++;
         catQueue.push( nullptr );
         cout << endl;
   
         if(catQueue.front() == nullptr) // end of queue
            return;
         else{
            cout << level << getEnglishSuffix( level ) << " Generation" << endl;
            continue;
         }
      }

      // add left and right cats of current cat
      if( cat->left != nullptr ){
         catQueue.push( cat->left );
      }
      if( cat->right != nullptr ){
         catQueue.push( cat->right );
      }

      cout << "  " << cat->name;
   }
}

const char* CatEmpire::getEnglishSuffix( int n ) {
   static const char suffixes [][3] = {"th", "st", "nd", "rd"};
   auto ord = n % 100;
   
   if (ord / 10 == 1) 
      ord = 0;
        
   ord = ord % 10;
        
   if (ord > 3) 
      ord = 0;
        
   return suffixes[ord];
}
